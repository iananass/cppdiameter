#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>


#include <thread>
#include <chrono>
#include "Diameter/DiameterPacket.h"
#include "Diameter/DiameterClient.h"
#include "Diameter/Flags.h"


DiameterPacket CER()
{
    DiameterPacket p(Capabilities_Exchange_Request, Diameter_Common_Messages, CommandFlags(RequestFlag::yes),
                     HopByHopID(432), EndToEndID(534));

    p.AppendAVP(Session_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("03RadiusInitiated;996555905051;-3;1481616943"));
    p.AppendAVP(Origin_Host, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("localhost"));
    p.AppendAVP(Origin_Realm, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("My test diameter realm"));
    p.AppendAVP(Host_IP_Address, AVPFlags(MandatoryFlag::yes), VendorID(0), DiameterIPv4Address(0x0100007f));
    p.AppendAVP(Vendor_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(0x00112233));
    p.AppendAVP(Product_Name, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("Perfect DPI"));
    p.AppendAVP(Destination_Realm, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("mobicents.org"));
    p.AppendAVP(Origin_State_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(0x44556677));
    p.AppendAVP(Auth_Application_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(Diameter_Credit_Control));


    return p;
}

DiameterPacket CCR()
{
    DiameterPacket p(Credit_Control_Request, Diameter_Credit_Control, CommandFlags(RequestFlag::yes),
                     HopByHopID(432), EndToEndID(534));

    p.AppendAVP(Session_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("03RadiusInitiated;996555905051;-3;1481616943"));
    p.AppendAVP(Origin_Host, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("localhost"));
    p.AppendAVP(Origin_Realm, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("mobicents.org"));
    p.AppendAVP(Destination_Realm, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("mobicents.org"));
    p.AppendAVP(Host_IP_Address, AVPFlags(MandatoryFlag::yes), VendorID(0), DiameterIPv4Address(0x0100007f));
    p.AppendAVP(Vendor_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(0x00112233));
    p.AppendAVP(Product_Name, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("Perfect DPI"));
    p.AppendAVP(Service_Context_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), std::string("ADM"));
    p.AppendAVP(Origin_State_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(0x44556677));
    p.AppendAVP(Auth_Application_Id, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(Diameter_Credit_Control));
    p.AppendAVP(CC_Request_Type, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(Initial_Request));

    p.AppendAVP(CC_Request_Number, AVPFlags(MandatoryFlag::yes, VendorFlag::yes), VendorID(0), u_int32_t(432));
    {
        auto final = p.BeginGroupedAVP(Multiple_Services_Credit_Control, AVPFlags(MandatoryFlag::yes), VendorID(0));
        p.AppendAVP(Rating_Group, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int32_t(102));
        {
            auto final2 = p.BeginGroupedAVP(Used_Service_Unit, AVPFlags(MandatoryFlag::yes), VendorID(0));
            p.AppendAVP(CC_Input_Octets, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int64_t(123));
            p.AppendAVP(CC_Output_Octets, AVPFlags(MandatoryFlag::yes), VendorID(0), u_int64_t(456));

        }
    }
    p.AppendAVP(Event_Timestamp, AVPFlags(MandatoryFlag::yes), VendorID(0), int32_t(0x00000005));
    return p;
}

int main()
{

    DiameterClient dclient(htonl(INADDR_LOOPBACK), 3868);
//    dclient.SendRequest(CER());
//    std::this_thread::sleep_for(std::chrono::seconds(2));


    dclient.SendRequest(CCR());
}