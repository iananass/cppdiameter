#ifndef DIAMETER_FLAGS_H
#define DIAMETER_FLAGS_H

enum class VendorFlag{yes, no};
enum class MandatoryFlag{yes, no};
enum class ProtectionFlag{yes, no};

enum class RequestFlag{yes, no};
enum class ProxyableFlag{yes, no};
enum class ErrorFlag{yes, no};
enum class RetransmitFlag{yes, no};

struct DiameterAVP;

class AVPFlags
{
public:
    template <class... Flags>
    AVPFlags(Flags ... flags)
    {
        SetFlag(flags...);
    }

    void AVPSetFlags(DiameterAVP* avp);
    VendorFlag vFlag = VendorFlag::no;
    MandatoryFlag mFlag = MandatoryFlag::no;
    ProtectionFlag pFlag = ProtectionFlag::no;

private:

    template <class F, class ... FLAGS>
    void SetFlag(F f, FLAGS ... flags)
    {
        SetFlag(f);
        SetFlag(flags...);
    };

    void SetFlag(VendorFlag f)     { vFlag = f; }
    void SetFlag(MandatoryFlag f)  { mFlag = f; }
    void SetFlag(ProtectionFlag f) { pFlag = f; }
};


class CommandFlags
{
public:
    template<class... Flags>
    CommandFlags(Flags ... flags)
    {
        SetFlag(flags...);
    }

    void AVPSetFlags(DiameterAVP* avp);

    void CommandSetFlags(struct DiameterHdr* hdr);
    RequestFlag rFlag = RequestFlag::no;
    ProxyableFlag pFlag = ProxyableFlag::no;
    ErrorFlag eFlag = ErrorFlag::no;
    RetransmitFlag tFlag = RetransmitFlag::no;
private:

    template <class F, class ... FLAGS>
    void SetFlag(F f, FLAGS ... flags)
    {
        SetFlag(f);
        SetFlag(flags...);
    };

    void SetFlag(RequestFlag f)     { rFlag = f; }
    void SetFlag(ProxyableFlag f)   { pFlag = f; }
    void SetFlag(ErrorFlag f)       { eFlag = f; }
    void SetFlag(RetransmitFlag f)  { tFlag = f; }
};

#endif //DIAMETER_FLAGS_H
