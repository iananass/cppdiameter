#include "DiameterClient.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <iostream>

DiameterClient::DiameterClient(u_int32_t servIP, u_int16_t servPort)
: m_servIP(servIP)
, m_servPort(htons(servPort))
{
    struct sockaddr_in addr;

    m_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (m_socket < 0) {
        std::cerr << "sock invalid" << std::endl;
        return ;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = m_servPort;
    addr.sin_addr.s_addr = m_servIP;
    if (connect(m_socket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
        std::cerr << "connect error" << std::endl;
        m_socket = -1;
        return;
    }
}

DiameterClient::~DiameterClient()
{
    if (ConnectionIsOK()) {
        close(m_socket);
    }
}

DiameterPacket DiameterClient::SendRequest(const DiameterPacket& p)
{
    p.Send(m_socket);

    u_char buf[2048];

    auto bytes_read = recv(m_socket, buf, sizeof(buf), 0);
    if (bytes_read > 0)
        return DiameterPacket (buf, bytes_read);
    return DiameterPacket();
}

bool DiameterClient::ConnectionIsOK() const
{
    return m_socket >= 0;
}
