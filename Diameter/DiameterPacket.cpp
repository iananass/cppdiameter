#include "DiameterPacket.h"

u_int32_t DiameterAVP::GetDataAsUInt32() const
{
    return ntohl(*reinterpret_cast<const u_int32_t*>(Data));
}

u_int64_t DiameterAVP::GetDataAsUInt64() const
{
    return bswap_64(*reinterpret_cast<const u_int64_t*>(Data));
}

std::string DiameterAVP::GetDataAsString() const
{
    return std::string(reinterpret_cast<const char*>(Data), size_t(Len));
}

GroupedAVP::GroupedAVP(DiameterAVP* avp)
:m_avp(reinterpret_cast<u_char*>(avp))
{}

DiameterAVP* GroupedAVP::GetFirstAVP()
{
    m_offset = sizeof(DiameterAVP);
    if (OffsetOverflowed())
        return nullptr;
    return GetCurrentAVP();
}

const DiameterAVP* GroupedAVP::GetFirstAVP() const
{
    m_offset = sizeof(DiameterAVP);
    if (OffsetOverflowed())
        return nullptr;
    return GetCurrentAVP();
}

DiameterAVP* GroupedAVP::GetNextAVP()
{
    if (OffsetOverflowed())
        return nullptr;
    u_int32_t avpLen = GetCurrentAVP()->Len;
    avpLen = PadTo4(avpLen);
    m_offset += avpLen;
    if (OffsetOverflowed())
        return nullptr;
    return GetCurrentAVP();

}

const DiameterAVP* GroupedAVP::GetNextAVP() const
{
    if (OffsetOverflowed())
        return nullptr;
    u_int32_t avpLen = GetCurrentAVP()->Len;
    avpLen = PadTo4(avpLen);
    m_offset += avpLen;
    if (OffsetOverflowed())
        return nullptr;
    return GetCurrentAVP();
}

DiameterAVP* GroupedAVP::GetCurrentAVP()
{
    return reinterpret_cast<DiameterAVP*>(m_avp + m_offset);
}

const DiameterAVP* GroupedAVP::GetCurrentAVP() const
{
    return reinterpret_cast<const DiameterAVP*>(m_avp + m_offset);
}

const DiameterAVP* GroupedAVP::GetMainAVP() const
{
    return reinterpret_cast<const DiameterAVP*>(m_avp);
}

bool GroupedAVP::OffsetOverflowed() const
{
    return m_offset >= GetMainAVP()->Len;
}

u_int32_t PadTo4(u_int32_t i)
{
    if (i & 3) {
        return (i | 3) + 1;
    }
    return i;
}

DiameterPacket::GroupedAVPFinalizer::~GroupedAVPFinalizer()
{
    m_packet->EndGroupedAVP(m_avp);
}

DiameterPacket::GroupedAVPFinalizer::GroupedAVPFinalizer(DiameterAVP* avp, DiameterPacket* packet)
:m_avp(avp)
, m_packet (packet)
{}

DiameterPacket::DiameterPacket(DiameterCommandCode CommandCode, DiameterApplicationID AppID, CommandFlags flags, HopByHopID HbHID, EndToEndID EtEID)
{
    auto hdr = reinterpret_cast<DiameterHdr*> (m_data);
    hdr->Version =1 ;
    flags.CommandSetFlags(hdr);
    hdr->CommandCode = CommandCode;
    hdr->ApplicationID = AppID;
    hdr->HopByHopID = HbHID.id;
    hdr->EndToEndID = EtEID.id;
}

DiameterPacket::DiameterPacket(u_int8_t* extData, size_t len)
{
    memcpy(m_data, extData, std::min(len, sizeof(m_data)));
}

void DiameterPacket::AppendAVP(DiameterAVPCode code, AVPFlags flags, VendorID vendor, std::string data)
{
    auto avp = reinterpret_cast<DiameterAVP*> (m_data + m_offset);
    u_int32_t avpLen = sizeof(DiameterAVP) + data.length() + (flags.vFlag == VendorFlag::yes ? 4 : 0);
    u_int32_t avpLenPadded = PadTo4(avpLen);
    if (m_offset + avpLenPadded > sizeof(m_data))
        return;

    flags.AVPSetFlags(avp);
    avp->Code = code;
    avp->Len = avpLen;
    u_int32_t localOffset = sizeof(DiameterAVP);
    if (flags.vFlag == VendorFlag::yes) {
        reinterpret_cast<VendorID*>(m_data + m_offset + localOffset)->id = htonl(vendor.id);
        localOffset += sizeof(vendor.id);
    }
    memcpy(m_data + m_offset + localOffset, data.data(), data.length());
    Shift(avpLenPadded);
    Header()->MessageLen = m_offset;
}

DiameterPacket::GroupedAVPFinalizer&& DiameterPacket::BeginGroupedAVP(DiameterAVPCode code, AVPFlags flags, VendorID vendor)
{
    auto avp = reinterpret_cast<DiameterAVP*> (m_data + m_offset);
    flags.AVPSetFlags(avp);
    avp->Code = code;
    return std::move(GroupedAVPFinalizer(avp, this));
}

DiameterHdr* DiameterPacket::Header()
{
    return reinterpret_cast<DiameterHdr*> (m_data);
}

const DiameterHdr* DiameterPacket::Header() const
{
    return reinterpret_cast<const DiameterHdr*> (m_data);
}

DiameterAVP* DiameterPacket::GetFirstAVP()
{
    m_offset = sizeof(DiameterHdr);
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    return GetCurrentAVP();
}

const DiameterAVP* DiameterPacket::GetFirstAVP() const
{
    m_offset = sizeof(DiameterHdr);
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    return GetCurrentAVP();
}

DiameterAVP* DiameterPacket::GetNextAVP()
{
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    u_int32_t avpLen = GetCurrentAVP()->Len;
    avpLen = PadTo4(avpLen);
    m_offset += avpLen;
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    return GetCurrentAVP();
}

const DiameterAVP* DiameterPacket::GetNextAVP() const
{
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    u_int32_t avpLen = GetCurrentAVP()->Len;
    avpLen = PadTo4(avpLen);
    m_offset += avpLen;
    if (m_offset >= Header()->MessageLen)
        return nullptr;
    return GetCurrentAVP();
}

void DiameterPacket::Send(int socket) const
{
    send(socket, m_data, m_offset, 0);
}

void DiameterPacket::Shift(size_t n)
{
    m_offset += n;
}

void DiameterPacket::EndGroupedAVP(DiameterAVP* avp)
{
    avp->Len = m_offset - (reinterpret_cast<u_int8_t*>(avp) - m_data);
}

DiameterAVP* DiameterPacket::GetCurrentAVP()
{
    return reinterpret_cast<DiameterAVP*>(m_data + m_offset);
}
const DiameterAVP* DiameterPacket::GetCurrentAVP() const
{
    return reinterpret_cast<const DiameterAVP*>(m_data + m_offset);
}

void ToNetworkByteOrder(u_int32_t& val)
{
    val = htonl(val);
}
void ToNetworkByteOrder(u_int64_t& val)
{
    val = bswap_64(val);
}
void ToNetworkByteOrder(DiameterIPv4Address&)
{}
