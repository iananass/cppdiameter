#ifndef DIAMETER_COMMANDCODES_H_H
#define DIAMETER_COMMANDCODES_H_H

enum DiameterCommandCode
{
    // rfc3588
    Abort_Session_Request = 274,
    Abort_Session_Answer = 274,
    Accounting_Request = 271,
    Accounting_Answer = 271,
    Capabilities_Exchange_Request = 257,
    Capabilities_Exchange_Answer = 257,
    Device_Watchdog_Request = 280,
    Device_Watchdog_Answer = 280,
    Disconnect_Peer_Request = 282,
    Disconnect_Peer_Answer = 282,
    Re_Auth_Request = 258,
    Re_Auth_Answer = 258,
    Session_Termination_Request = 275,
    Session_Termination_Answer = 275,

    // rfc4006
    Credit_Control_Request = 272,
    Credit_Control_Answer = 272

};

#endif //DIAMETER_COMMANDCODES_H_H
