#include "BigEndian.h"
#include <netinet/in.h>


u_int16_t_be::u_int16_t_be(u_int16_t i) : m_data(htons(i))
{}

u_int16_t_be::operator u_int16_t() const
{ return ntohl(m_data); }

void u_int16_t_be::operator=(u_int16_t o)
{ m_data = htons(o); }


u_int24_t_be::u_int24_t_be(u_int32_t i)
{
    m_data[0] = (i >> 16) & 0xff;
    m_data[1] = (i >> 8) & 0xff;
    m_data[2] = i & 0xff;
}

u_int24_t_be::operator u_int32_t() const
{
    u_int32_t ret = m_data[0];
    ret <<= 8;
    ret |= m_data[1];
    ret <<= 8;
    ret |= m_data[2];
    return ret;
}

void u_int24_t_be::operator=(u_int32_t i)
{
    m_data[0] = (i >> 16) & 0xff;
    m_data[1] = (i >> 8) & 0xff;
    m_data[2] = i & 0xff;
}


u_int32_t_be::u_int32_t_be(u_int32_t i) : m_data(htonl(i))
{}

u_int32_t_be::operator u_int32_t() const
{ return ntohl(m_data); }

void u_int32_t_be::operator=(u_int32_t o)
{ m_data = htonl(o); }