#ifndef DIAMETER_DIAMETERAPPLICATIONID_H
#define DIAMETER_DIAMETERAPPLICATIONID_H

enum DiameterApplicationID
{
    Diameter_Common_Messages = 0,
    NASREQ = 1,
    Mobile_IP = 2,
    Diameter_Base_Accounting = 3,
    Diameter_Credit_Control = 4
};

#endif //DIAMETER_DIAMETERAPPLICATIONID_H
