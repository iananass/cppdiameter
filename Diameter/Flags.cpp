#include "Flags.h"
#include "DiameterPacket.h"

#define SetFlag(cond, flag) if (cond) flag = 1; else flag = false

void AVPFlags::AVPSetFlags(DiameterAVP* avp)
{
    SetFlag(vFlag == VendorFlag::yes, avp->vFlag);
    SetFlag(mFlag == MandatoryFlag::yes, avp->mFlag);
    SetFlag(pFlag == ProtectionFlag::yes, avp->pFlag);
    avp->notUsed = 0;
}

void CommandFlags::CommandSetFlags(struct DiameterHdr* hdr)
{
    SetFlag(rFlag == RequestFlag::yes,hdr->rFlag);
    SetFlag(pFlag == ProxyableFlag::yes,hdr->pFlag);
    SetFlag(eFlag == ErrorFlag::yes,hdr->eFlag);
    SetFlag(tFlag == RetransmitFlag::yes,hdr->tFlag);
    hdr->notUsed = 0;
}