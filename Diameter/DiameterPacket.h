#ifndef DIAMETER_DIAMETERPACKET_H
#define DIAMETER_DIAMETERPACKET_H

#include <sys/types.h>
#include <string>
#include <cstring>
#include <netinet/in.h>
#include <byteswap.h>
#include "Flags.h"
#include "DiameterApplicationID.h"
#include "BigEndian.h"
#include "AVPCodes.h"
#include "CommandCodes.h"

struct DiameterHdr
{
    u_int8_t Version;
    u_int24_t_be MessageLen;
    u_int8_t notUsed:4;
    u_int8_t tFlag:1;
    u_int8_t eFlag:1;
    u_int8_t pFlag:1;
    u_int8_t rFlag:1;
    u_int24_t_be CommandCode;
    u_int32_t_be ApplicationID;
    u_int32_t_be HopByHopID;
    u_int32_t_be EndToEndID;
} __attribute__ ((packed));


struct DiameterAVP
{
    u_int32_t_be Code;
    u_int8_t notUsed:5;
    u_int8_t pFlag:1;
    u_int8_t mFlag:1;
    u_int8_t vFlag:1;
    u_int24_t_be Len;
    u_int8_t Data[];

    u_int32_t GetDataAsUInt32() const;
    u_int64_t GetDataAsUInt64() const;
    std::string GetDataAsString() const;
} __attribute__ ((packed));

class GroupedAVP
{
public:
    GroupedAVP(DiameterAVP*);

    DiameterAVP* GetFirstAVP();
    const DiameterAVP* GetFirstAVP() const;
    DiameterAVP* GetNextAVP();
    const DiameterAVP* GetNextAVP() const;
private:
    u_char* m_avp;
    mutable size_t m_offset = sizeof(DiameterAVP);
    DiameterAVP* GetCurrentAVP();
    const DiameterAVP* GetCurrentAVP() const;
    const DiameterAVP* GetMainAVP() const;
    bool OffsetOverflowed() const;
};

u_int32_t PadTo4(u_int32_t i);

struct HopByHopID
{
    u_int32_t id;
    HopByHopID() = default;
    explicit HopByHopID(u_int32_t i) : id (i) {}
};

struct EndToEndID
{
    u_int32_t id;
    EndToEndID() = default;
    explicit EndToEndID(u_int32_t i) : id (i) {}
};

struct VendorID
{
    u_int32_t id;
    VendorID() = default;
    explicit VendorID(u_int32_t i) : id (i) {}
};

struct DiameterIPv4Address
{
    u_int8_t family[2] = {0,1};
    u_int32_t ip;

    DiameterIPv4Address(u_int32_t addr) : ip(addr) {}
}__attribute__ ((packed));

template <class T>
void ToNetworkByteOrder(T& t);

class DiameterPacket
{
public:
    class GroupedAVPFinalizer
    {
    public:
        ~GroupedAVPFinalizer();
    private:
        GroupedAVPFinalizer(DiameterAVP* avp, DiameterPacket* packet);
        DiameterAVP* m_avp;
        DiameterPacket* m_packet;
        friend class DiameterPacket;
    };
    enum {MaxDiameterPacketLen = 2048};

    DiameterPacket() = default;
    DiameterPacket(DiameterCommandCode CommandCode, DiameterApplicationID AppID, CommandFlags flags,
                   HopByHopID HbHID, EndToEndID EtEID);
    DiameterPacket(u_int8_t* extData, size_t len);

    void AppendAVP(DiameterAVPCode code, AVPFlags flags, VendorID vendor, std::string data);
    template <class T>
    void AppendAVP(DiameterAVPCode code, AVPFlags flags, VendorID vendor, T data)
    {
        auto avp = reinterpret_cast<DiameterAVP*> (m_data + m_offset);
        u_int32_t avpLen = sizeof(DiameterAVP) + sizeof(data) + (flags.vFlag == VendorFlag::yes ? 4 : 0);
        u_int32_t avpLenPadded = PadTo4(avpLen);
        if (m_offset + avpLenPadded > sizeof(m_data))
            return;

        flags.AVPSetFlags(avp);
        avp->Code = code;
        avp->Len = avpLen;
        u_int32_t localOffset = sizeof(DiameterAVP);
        if (flags.vFlag == VendorFlag::yes) {
            reinterpret_cast<VendorID*>(m_data + m_offset + localOffset)->id = htonl(vendor.id);
            localOffset += sizeof(vendor.id);
        }
        memcpy(m_data + m_offset + localOffset, &data, sizeof(data));
        Shift(avpLenPadded);
        Header()->MessageLen = m_offset;
    }
    GroupedAVPFinalizer&& BeginGroupedAVP(DiameterAVPCode code, AVPFlags flags, VendorID vendor);



    DiameterHdr* Header();
    const DiameterHdr* Header() const;
    DiameterAVP* GetFirstAVP();
    const DiameterAVP* GetFirstAVP() const;
    DiameterAVP* GetNextAVP();
    const DiameterAVP* GetNextAVP() const;

    void Send(int socket) const;

    friend class GroupedAVPFinalizer;
private:

    void Shift(size_t n);
    void EndGroupedAVP(DiameterAVP* avp);
    DiameterAVP* GetCurrentAVP();
    const DiameterAVP* GetCurrentAVP() const;

    u_int8_t m_data[MaxDiameterPacketLen];

    mutable size_t m_offset = sizeof(DiameterHdr);

};


#endif //DIAMETER_DIAMETERPACKET_H
