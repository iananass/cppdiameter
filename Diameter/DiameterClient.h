
#ifndef DIAMETER_DIAMETERCLIENT_H
#define DIAMETER_DIAMETERCLIENT_H

#include <sys/types.h>
#include "DiameterPacket.h"

class DiameterClient
{
public:
    DiameterClient(u_int32_t servIP, u_int16_t servPort);
    ~DiameterClient();

    DiameterPacket SendRequest(const DiameterPacket&);

    bool ConnectionIsOK() const;
private:
    u_int32_t m_servIP;
    u_int16_t m_servPort;
    int m_socket = -1;
};

#endif //DIAMETER_DIAMETERCLIENT_H
