#ifndef DIAMETER_U_INT24_T_H
#define DIAMETER_U_INT24_T_H

#include <sys/types.h>

struct u_int16_t_be
{
    u_int16_t_be(u_int16_t i);
    operator u_int16_t() const;
    void operator=(u_int16_t o);
    u_int16_t m_data;
} __attribute__ ((packed));

struct u_int24_t_be
{
    u_int24_t_be(u_int32_t i);
    operator u_int32_t() const;
    void operator=(u_int32_t o);
    u_int8_t m_data[3];
} __attribute__ ((packed));

struct u_int32_t_be
{
    u_int32_t_be(u_int32_t i);
    operator u_int32_t() const;
    void operator=(u_int32_t o);
    u_int32_t m_data;
} __attribute__ ((packed));

#endif //DIAMETER_U_INT24_T_H
